'use strict'
module.exports = {
  NODE_ENV: '"production"',
  API_URL: JSON.stringify(process.env.API_URL) || '"http://localhost:3000/api"',
  GMAPS_TOKEN: JSON.stringify(process.env.GMAPS_TOKEN)
}
